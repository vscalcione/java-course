package it.intersistemi.corsojava.exceptions.fruitexcercise;

public class TooHeavyException extends Exception{
    public TooHeavyException(String message) {
        super(message);
    }
}
