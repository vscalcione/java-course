package it.intersistemi.corsojava.exceptions.fruitexcercise;

public class PeachFruit extends Fruit{
    public PeachFruit(int weight) {
        super(weight);
    }
}
